#include <iostream>

#include "GoldBehaviour.h"

GoldBehaviour::GoldBehaviour()
	: m_id(0)
	, m_pWorker(nullptr)
{
	// empty..
}

bool GoldBehaviour::IsBusyOrOccupied()
{
	return m_pWorker != nullptr;
}

void GoldBehaviour::PrintId()
{
	std::cout << m_id;
}
