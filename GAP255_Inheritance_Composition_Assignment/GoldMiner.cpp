#include "GoldMiner.h"
#include <iostream>
#include "GoldVein.h"

GoldMiner::GoldMiner()
    : GoldBehaviour()
    , m_skill{0}
{
}

void GoldMiner::Init(int id, int skill)
{
    m_id = id;
    m_skill = skill;
    std::cout << "GoldMiner " << id << " hired.\n";
}

void GoldMiner::Assign(GoldBehaviour* pVein)
{
    m_pWorker = pVein;
}

int GoldMiner::Mine()
{
    if (m_pWorker != nullptr)
    {
        int mined = GetForm()->Mine(m_skill);
        std::cout << "Miner " << m_id << " mined " << mined << '\n';
        return mined;
    }
    return 0;
}

void GoldMiner::Print()
{
    std::cout << "GoldMiner " << m_id << ", skill: " << m_skill;
    if (m_pWorker != nullptr)
    {
        std::cout << ", currently mining mine ";
        m_pWorker->PrintId();
    }
}


GoldVein* GoldMiner::GetForm()
{
    return dynamic_cast<GoldVein*>(m_pWorker);
}