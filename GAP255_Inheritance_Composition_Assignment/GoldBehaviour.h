#pragma once

class GoldBehaviour
{
protected:
	int m_id;
	GoldBehaviour* m_pWorker;

public:
	GoldBehaviour();

	bool IsBusyOrOccupied();

	virtual void Init(int, int) = 0;
	virtual void Assign(GoldBehaviour* pMiner) = 0;

	virtual void Print() = 0;
	void PrintId();
};

