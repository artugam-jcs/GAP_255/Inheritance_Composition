#include "GoldVein.h"
#include <iostream>
#include "GoldMiner.h"

GoldVein::GoldVein()
    : GoldBehaviour()
    , m_quantity{0}
{
}

void GoldVein::Init(int id, int quantity)
{
    m_id = m_quantity;
    m_quantity = quantity;
}

void GoldVein::Assign(GoldBehaviour* pMiner)
{
    m_pWorker = pMiner;
}

int GoldVein::Mine(int quantity)
{
    int output;
    if (m_quantity > quantity)
    {
        m_quantity -= quantity;
        output = quantity;
    }
    else
    {
        output = m_quantity;
        m_quantity = 0;
        std::cout << "Vein " << m_id << " is depleted!\n";
    }
    return output;
}

void GoldVein::Print()
{
    std::cout << "GoldVein " << m_id << " has " << m_quantity << " units of gold";
    if (m_pWorker != nullptr)
    {
        std::cout << ", currently mined by miner ";
        m_pWorker->PrintId();
    }
}
