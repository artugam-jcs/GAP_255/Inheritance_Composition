#pragma once
#include "GoldMiner.h"
#include "GoldVein.h"

class IdleMinerGame
{
private:
    int m_playerGold;
    int m_turnCounter;

    GoldMiner m_goldDiggers[10];
    int m_goldDiggerCount;

    GoldVein m_goldVeins[10];
    int m_goldVeinCount;

public:
    IdleMinerGame();
    void Play();

private:
    bool AreAllMinersBusy();
    bool AreAllVeinsOccupied();

    void Hire(int cost);
    void FindMine();
    void Assign();
};