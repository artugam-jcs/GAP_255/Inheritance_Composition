#pragma once

#include "GoldBehaviour.h"

class GoldVein;

class GoldMiner : public GoldBehaviour
{
private:
    int m_skill;

public:
    GoldMiner();

    void Init(int id, int skill) override;
    void Assign(GoldBehaviour* pVein) override;
    int Mine();

    void Print() override;

private:
    GoldVein* GetForm();
};