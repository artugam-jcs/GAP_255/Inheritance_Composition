#pragma once

#include "GoldBehaviour.h"

class GoldVein : public GoldBehaviour
{
private:
    int m_quantity;

public:
    GoldVein();

    void Init(int id, int quantity) override;
    void Assign(GoldBehaviour* pMiner) override;
    int Mine(int quantity);

    void Print() override;
};

